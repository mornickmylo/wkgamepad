#include "game-window-getter.h"

DWORD currentProcessId = NULL;
HWND result = NULL;

BOOL CALLBACK EnumWindowsProc(
	_In_ HWND   hwndOfIteration,
	_In_ LPARAM lParam
) {
	DWORD processId = 0;
	GetWindowThreadProcessId(hwndOfIteration, &processId);
	if (processId == currentProcessId) {
		result = hwndOfIteration;
		return FALSE;
	}
	return TRUE;
}

void GetGameWindow(HWND* pGameWindow) {
	currentProcessId = GetCurrentProcessId();
	EnumWindows(&EnumWindowsProc, NULL);
	*pGameWindow = result;
}