#include <map>
#include <set>
#include <string>

typedef struct {
	std::set<std::string> gamepadBoolState;
	std::map<std::string, int16_t> gamepadNumericState;
	bool shallReturn;
	bool shallHandle;
} DInputGamepadState;