#include "direct-input-adapter.h"
#include "game-window-getter.h"
#include "../defaults.h"
#include <iostream>
#include <strsafe.h>
#include <winerror.h>
#include <comdef.h>

DirectInputAdapter::DirectInputAdapter(bool& initError, uint8_t index, HWND gameWindowParam)
{
	targetDeviceIndex = index;
	initError = false;

	povToString = {
		{0, {"POVU"}},
		{4500, {"POVU", "POVR"}},
		{9000, {"POVR"}},
		{13500, {"POVR","POVD"}},
		{18000, {"POVD"}},
		{22500, {"POVD","POVL"}},
		{27000, {"POVL"}},
		{31500, {"POVL", "POVU"}}
	};

	HRESULT hr;

	if (FAILED(hr = DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION,
		IID_IDirectInput8, (VOID**)&pDirectInput, nullptr)))
	{
		printf("Cannot create DirectInput!\n");
		initError = true;
		return;
	}

	gameWindow = gameWindowParam;

	if (FAILED(hr = pDirectInput->EnumDevices(DI8DEVCLASS_GAMECTRL,
		EnumGamepadsCallback,
		this, DIEDFL_ATTACHEDONLY))) {
		printf("Failed to enumerate DInput devices\n");
		initError = true;
		return;
	}

	initError = pGamepad == nullptr;
}

DirectInputAdapter::~DirectInputAdapter()
{
	if (pGamepad != nullptr) {
		pGamepad->Unacquire();
	}
}

DInputGamepadState DirectInputAdapter::GetGamepadState()
{
    static HRESULT hr;
	static DIJOYSTATE gamepadState;
    
    DInputGamepadState dinputGamepadState;
    
	hr = pGamepad->Poll();

	if (FAILED(hr)) {
		printf("Failed to poll gamepad state: %lu\n", GetLastError());
        dinputGamepadState.shallHandle = false;
		this->TryReacquire(hr, dinputGamepadState);
		return dinputGamepadState;
	}

	hr = pGamepad->GetDeviceState(sizeof(DIJOYSTATE),
		(LPVOID)&gamepadState);

	if (FAILED(hr)) {
		printf("Failed to get gamepad state: %lu\n", GetLastError());
        dinputGamepadState.shallHandle = false;
		this->TryReacquire(hr, dinputGamepadState);
		return dinputGamepadState;
	}

    dinputGamepadState.gamepadBoolState.clear();
	if (gamepadState.rgdwPOV[0] < 36000) {
		auto gamepadBoolStateEnd = dinputGamepadState.gamepadBoolState.end();
		for (const auto& str : this->povToString[gamepadState.rgdwPOV[0]]) {
            dinputGamepadState.gamepadBoolState.insert(gamepadBoolStateEnd, str);
		}
	}

	for (uint8_t i = 0; i < 32; i++) {
		if (gamepadState.rgbButtons[i] != 0) {
            dinputGamepadState.gamepadBoolState.insert("Button" + std::to_string(i));
		}
	}
	dinputGamepadState.gamepadNumericState["X"] = gamepadState.lX;
	dinputGamepadState.gamepadNumericState["Y"] = gamepadState.lY;
	dinputGamepadState.gamepadNumericState["Z"] = gamepadState.lZ;
	dinputGamepadState.gamepadNumericState["RX"] = gamepadState.lRx;
	dinputGamepadState.gamepadNumericState["RY"] = gamepadState.lRy;
	dinputGamepadState.gamepadNumericState["RZ"] = gamepadState.lRz;

	InsertAxisBoolState("X", gamepadState.lX, dinputGamepadState.gamepadBoolState);
	InsertAxisBoolState("Y", gamepadState.lY, dinputGamepadState.gamepadBoolState);
	InsertAxisBoolState("Z", gamepadState.lZ, dinputGamepadState.gamepadBoolState);
	InsertAxisBoolState("RX", gamepadState.lRx, dinputGamepadState.gamepadBoolState);
	InsertAxisBoolState("RY", gamepadState.lRy, dinputGamepadState.gamepadBoolState);
	InsertAxisBoolState("RZ", gamepadState.lRz, dinputGamepadState.gamepadBoolState);

    dinputGamepadState.shallHandle = true;
    dinputGamepadState.shallReturn = false;
    return dinputGamepadState;
}
void DirectInputAdapter::InsertAxisBoolState(const std::string& axisName, LONG axisValue, std::set<std::string>& gamepadBoolState) {
	if (axisValue < -DEFAULT_JOYSTICK_BOOL_STATE_DEADZONE) {
		gamepadBoolState.insert("Axis" + axisName + "-");
	}
	else if (axisValue > DEFAULT_JOYSTICK_BOOL_STATE_DEADZONE) {
		gamepadBoolState.insert("Axis" + axisName + "+");
	}
}
void DirectInputAdapter::TryReacquire(HRESULT previousFail, DInputGamepadState& dinputGamepadState) {
	if (previousFail != DIERR_INPUTLOST && previousFail != DIERR_NOTACQUIRED)
	{
        dinputGamepadState.shallHandle = false;
        dinputGamepadState.shallReturn = false;
		return;
	}

	// ����� ����������� ��������� � ��������
	// �������� ������ ��� ���
	if (FAILED(pGamepad->Acquire()))
	{
        dinputGamepadState.shallHandle = false;
        dinputGamepadState.shallReturn = false;
		return;
	}
}
bool DirectInputAdapter::TryInitGamepad(const DIDEVICEINSTANCE* pdidInstance)
{
	printf("Found Device: %s\n", pdidInstance->tszInstanceName);

	if (devicesFound != targetDeviceIndex) {
		devicesFound++;
		return false;
	}

	if (this->InitGamepadObject(pdidInstance->guidInstance)
		&& this->SetDataFormat()
		&& this->SetCooperativeLevel()
		&& this->SetRanges()
		&& this->SetDeadZones()
		&& this->Acquire())
	{
		printf("Device %s initialization success! \n", pdidInstance->tszInstanceName);
		return true;
	}
	return false;
}
bool DirectInputAdapter::InitGamepadObject(GUID gamepadInstance)
{
	HRESULT hr = pDirectInput->CreateDevice(gamepadInstance, &pGamepad, nullptr);

	// If it failed, then we can't use this joystick. (Maybe the user unplugged
	// it while we were in the middle of enumerating it.)
	if (FAILED(hr)) {
		pGamepad->Release();
		printf("Error creating device object: %lu\n", GetLastError());
		pGamepad = nullptr;
		return false;
	}
	return true;
}
bool DirectInputAdapter::SetDataFormat()
{
	HRESULT hr;
	hr = pGamepad->SetDataFormat(&c_dfDIJoystick);

	if (FAILED(hr)) {
		pGamepad->Release();
		printf("Error setting data format: %lu\n", GetLastError());
		pGamepad = nullptr;
		return false;
	}
	return true;
}
bool DirectInputAdapter::SetCooperativeLevel()
{
	HRESULT hr;
	hr = pGamepad->SetCooperativeLevel(gameWindow, DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);

	if (FAILED(hr)) {
		pGamepad->Release();
		printf("Error setting cooperative level: %lu\n", GetLastError());
		pGamepad = nullptr;
		return false;
	}
	return true;
}
bool DirectInputAdapter::SetRanges()
{
	DIPROPRANGE dipr;

	// ������� ������� ���������
	ZeroMemory(&dipr, sizeof(DIPROPRANGE));

	dipr.diph.dwSize = sizeof(DIPROPRANGE);
	dipr.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	dipr.diph.dwObj = DIJOFS_X;
	dipr.diph.dwHow = DIPH_BYOFFSET; // �������� � ������� ������

	dipr.lMin = INT16_MIN;
	dipr.lMax = INT16_MAX;

	const uint8_t axisCount = 6;

	DWORD axisDIPropHeaderOffsets[axisCount] = { DIJOFS_X, DIJOFS_Y, DIJOFS_Z, DIJOFS_RX, DIJOFS_RY, DIJOFS_RZ };
	const char axisNames[axisCount][3] = { "X", "Y", "Z", "RX", "RY", "RZ" };
	for (int i = 0; i < axisCount; i++) {
		dipr.diph.dwObj = axisDIPropHeaderOffsets[i];
		if (FAILED(pGamepad->SetProperty(DIPROP_RANGE,
			&dipr.diph))) {
			printf("Error setting range for axis %s: %lu\n", axisNames[i], GetLastError());
		}
	}

	return true;
}
bool DirectInputAdapter::SetDeadZones()
{
	DIPROPDWORD dwDirectInputProperty;

	// ������� ������� ���������
	ZeroMemory(&dwDirectInputProperty, sizeof(DIPROPDWORD));

	dwDirectInputProperty.diph.dwSize = sizeof(DIPROPDWORD);
	dwDirectInputProperty.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	dwDirectInputProperty.diph.dwHow = DIPH_BYOFFSET; // �������� � ������� ������

	dwDirectInputProperty.dwData = JOYSTICK_DIRECT_INPUT_DEADZONE;

	const uint8_t axisCount = 6;

	DWORD axisDIPropHeaderOffsets[axisCount] = { DIJOFS_X, DIJOFS_Y, DIJOFS_Z, DIJOFS_RX, DIJOFS_RY, DIJOFS_RZ };
	const char axisNames[axisCount][3] = { "X", "Y", "Z", "RX", "RY", "RZ" };

	for (int i = 0; i < axisCount; i++) {
		dwDirectInputProperty.diph.dwObj = axisDIPropHeaderOffsets[i];
		if (FAILED(pGamepad->SetProperty(DIPROP_DEADZONE,
			&dwDirectInputProperty.diph))) {
			printf("Error setting deadzone for axis %s: %lu\n", axisNames[i],  GetLastError());
		}
	}

	return true;
}
bool DirectInputAdapter::Acquire()
{
	HRESULT hr = pGamepad->Acquire();
	if (FAILED(hr)) {
        printf("Acquire gamepad failed, HRESULT=%ld", hr);
        pGamepad->Release();
        pGamepad = nullptr;
		return false;
	}
	return true;
}
BOOL CALLBACK EnumGamepadsCallback(const DIDEVICEINSTANCE* pdidInstance,
	VOID* pContext) {

	auto* pDirectInputAdapter = (DirectInputAdapter*)pContext;

	return pDirectInputAdapter->TryInitGamepad(pdidInstance) ? DIENUM_STOP : DIENUM_CONTINUE;
}
