#include "dinput.h"
#include <stdio.h>
#include <cstdint>
#include "dinput-gamepad-state.h"


BOOL CALLBACK EnumGamepadsCallback(const DIDEVICEINSTANCE* pdidInstance,
	VOID* pGameWindow);

class DirectInputAdapter
{
public:
	DirectInputAdapter(bool& initError, uint8_t index, HWND gameWindowParam);
	~DirectInputAdapter();
    DInputGamepadState GetGamepadState();
	bool TryInitGamepad(const DIDEVICEINSTANCE* pdidInstance);

private:
	LPDIRECTINPUT8 pDirectInput = nullptr;
	LPDIRECTINPUTDEVICE8A pGamepad = nullptr;
	uint8_t devicesFound = 0;
	uint8_t targetDeviceIndex = 0;
	HWND gameWindow = NULL;
	std::map<DWORD, std::set<std::string>> povToString;

	void TryReacquire(HRESULT previousFail, DInputGamepadState& dinputGamepadState);
	bool InitGamepadObject(GUID gamepadInstance);
	bool SetDataFormat();
	bool SetCooperativeLevel();
	bool SetRanges();
	bool SetDeadZones();
	bool Acquire();
	static void InsertAxisBoolState(const std::string& axisName, LONG axisValue, std::set<std::string>& gamepadBoolState);
};