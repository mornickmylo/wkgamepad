#include <string>
#include <set>
#include <iostream>
#include <unordered_set>
#include "two-keys-game-function.h"
#include "../WAAPI/KeyPressSimulator.h"

TwoKeysGameFunction::TwoKeysGameFunction(std::unordered_set<uint8_t> firstKeyWithModifiers, std::unordered_set<uint8_t> secondKeyWithModifiers, std::set<std::string> trigger)
{
    this->firstKeyWithModifiersCodes = firstKeyWithModifiers;
    this->secondKeyWithModifiersCodes = secondKeyWithModifiers;
    this->state = false;
    this->_trigger = trigger;
    this->isSecondKey = false;
}
void TwoKeysGameFunction::SetState(bool newState)
{
    if (newState != this->state) {
        if (newState) {
            this->isSecondKey = !this->isSecondKey;
        }
        for (const auto& neededButton : this->GetNeededButtons()) {
            KeyPressSimulator::SimulateState(neededButton, newState);
        }
        this->state = newState;
    }
}
std::unordered_set<uint8_t> TwoKeysGameFunction::GetNeededButtons()
{
    return this->isSecondKey ? this->secondKeyWithModifiersCodes : this->firstKeyWithModifiersCodes;
}
std::set<std::string> TwoKeysGameFunction::GetTrigger()
{
    return this->_trigger;
}
