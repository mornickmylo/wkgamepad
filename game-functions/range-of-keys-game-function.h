#ifndef RANGE_GAME_FUNCTION
#define RANGE_GAME_FUNCTION

#include <string>
#include <set>
#include <map>
#include <vector>
#include <memory>

enum RangeOfKeysIndexAction {
    Stay,
    Increase,
    Decrease
};

class RangeOfKeysGameFunction {
protected:
    std::vector<std::vector<uint8_t>> keyCodes;
    std::map<RangeOfKeysIndexAction, bool> states;
    std::set<std::string> _triggerPrevious;
    std::set<std::string> _triggerNext;
    std::unique_ptr<std::set<std::string>> _triggerSame;
    bool haveTriggerSame;
    uint8_t index;
    std::vector<uint8_t> GetNeededButtons();
public:
    RangeOfKeysGameFunction(std::set<std::string> triggerPrevious, std::set<std::string> triggerNext, std::unique_ptr<std::set<std::string>> triggerSame);
    virtual void SetState(bool newState, RangeOfKeysIndexAction indexAction);
    std::set<std::string> GetTriggerPrevious();
    std::set<std::string> GetTriggerNext();
    std::set<std::string> GetTriggerSame();
    bool HasTriggerSame();
};
#endif

