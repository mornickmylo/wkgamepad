#include "single-key-game-function.h"
#include "../fake-input.h"
#include "../WAAPI/KeyPressSimulator.h"

SingleKeyGameFunction::SingleKeyGameFunction(std::unordered_set<uint8_t> keyWithModifiers, std::set<std::string> trigger)
{
    this->keyAndModifiersCodes = std::move(keyWithModifiers);
    this->state = false;
    this->_trigger = std::move(trigger);
}

std::set<std::string> SingleKeyGameFunction::GetTrigger()
{
    return this->_trigger;
}
void SingleKeyGameFunction::SetState(bool newState)
{
    if (this->state != newState)
    {
        this->state = newState;
        for (const auto& pressedKeyboardKey : this->keyAndModifiersCodes) {
            KeyPressSimulator::SimulateState(pressedKeyboardKey, newState);
        }
    }
}

