#include "range-of-keys-game-function.h"

class WeaponKeysGameFunction : public RangeOfKeysGameFunction
{
public:
	WeaponKeysGameFunction(
		std::set<std::string> triggerPrevious,
		std::set<std::string> triggerNext, std::unique_ptr<std::set<std::string>> triggerSame
	);
	void SetState(bool newState, RangeOfKeysIndexAction indexAction) override;
};
