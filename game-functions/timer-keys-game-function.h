#include "range-of-keys-game-function.h"

class TimerKeysGameFunction : public RangeOfKeysGameFunction
{
public:
	TimerKeysGameFunction(
		std::set<std::string> triggerPrevious,
		std::set<std::string> triggerNext
	);
	void SetState(bool newState, RangeOfKeysIndexAction indexAction);
};