#include <string>
#include <set>
#include <vector>
#include <iostream>
#include "range-of-keys-game-function.h"
#include "../WAAPI/KeyPressSimulator.h"

RangeOfKeysGameFunction::RangeOfKeysGameFunction(std::set<std::string> triggerPrevious, std::set<std::string> triggerNext, std::unique_ptr<std::set<std::string>> triggerSame)
{
    this->states = { {RangeOfKeysIndexAction::Decrease, false}, {RangeOfKeysIndexAction::Increase, false}, {RangeOfKeysIndexAction::Stay, false} };
    this->_triggerPrevious = triggerPrevious;
    this->_triggerNext = triggerNext;
    this->index = 1;
    if (triggerSame != nullptr) {
        this->_triggerSame = std::move(triggerSame);
        this->haveTriggerSame = true;
    }
    else {
        this->haveTriggerSame = false;
    }
}
void RangeOfKeysGameFunction::SetState(bool newState, RangeOfKeysIndexAction indexAction)
{
    if (newState != this->states[indexAction]) {
        if (newState) {
            if (this->index > 0 && indexAction == RangeOfKeysIndexAction::Decrease) {
                this->index--;
            } else if (this->index < this->keyCodes.size() - 1 && indexAction == RangeOfKeysIndexAction::Increase) {
                this->index++;
            }
        }
        for (const auto& neededButton : this->GetNeededButtons()) {
            KeyPressSimulator::SimulateState(neededButton, newState);
        }
        this->states[indexAction] = newState;
    }
}
std::vector<uint8_t> RangeOfKeysGameFunction::GetNeededButtons()
{
    return this->keyCodes[this->index];
}
std::set<std::string> RangeOfKeysGameFunction::GetTriggerNext()
{
    return this->_triggerNext;
}
std::set<std::string> RangeOfKeysGameFunction::GetTriggerPrevious()
{
    return this->_triggerPrevious;
}
std::set<std::string> RangeOfKeysGameFunction::GetTriggerSame()
{
    return *this->_triggerSame;
}
bool RangeOfKeysGameFunction::HasTriggerSame()
{
    return this->haveTriggerSame;
}


