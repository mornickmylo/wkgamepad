#ifndef MOUSE_MOVING_FUNCTION
#define MOUSE_MOVING_FUNCTION

#include <string>
#include <map>
#include <set>

enum MouseMovingDirection {
    Horizontal,
    Vertical
};

class MouseMovingFunction {
private:
    std::set<std::string> _trigger;
    std::string _dependency;
    MouseMovingDirection _mouseMovingDirection;
    double _maxSpeed;
    std::map<std::string, double (MouseMovingFunction::*)(double)> calsSpeedFunctions;
    std::string _curve;
    int inputMax;

    double CalcSpeedLinear(double value);
    double CalcSpeedQuadratic(double value);
    double CalcSpeedCubic(double value);
    double CalcSpeedQuadraticExtreme(double value);
    double CalcSpeedPower(double value);
public:
    MouseMovingFunction(MouseMovingDirection mouseMovingDirection, double maxSpeed, std::set<std::string> trigger, std::string dependency, std::string curve);
    void SetState(double newState);
    std::string GetDependency();
    std::set<std::string> GetTrigger();
};
#endif
