#include "mouse-key-function.h"
#include <iostream>
#include <string>
#include <set>
#include "../WAAPI/KeyPressSimulator.h"

MouseKeyGameFunction::MouseKeyGameFunction(MouseKey mouseKey, std::set<std::string> trigger)
{
    this->_key = mouseKey == MouseKey::Left ? VK_LBUTTON : VK_RBUTTON;
    this->state = false;
    this->_trigger = trigger;
}

std::set<std::string> MouseKeyGameFunction::GetTrigger()
{
    return this->_trigger;
}
void MouseKeyGameFunction::SetState(bool newState)
{
    if (this->state != newState)
    {
        this->state = newState;
        KeyPressSimulator::SimulateState(_key, newState);
    }
}
