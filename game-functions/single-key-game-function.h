#ifndef PRESSING_GAME_FUNCTION
#define PRESSING_GAME_FUNCTION

#include <string>
#include <set>
#include <unordered_set>

class SingleKeyGameFunction {
private:
    std::unordered_set<uint8_t> keyAndModifiersCodes;
    bool state;
    std::set<std::string> _trigger;
public:
    SingleKeyGameFunction(std::unordered_set<uint8_t> keyWithModifiers, std::set<std::string> trigger);
    void SetState(bool newState);
    std::set<std::string> GetTrigger();
};
#endif
