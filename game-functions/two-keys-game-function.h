#ifndef SWITCH_GAME_FUNCTION
#define SWITCH_GAME_FUNCTION

#include <string>
#include <set>
#include <unordered_set>

class TwoKeysGameFunction {
private:
    std::unordered_set<uint8_t> firstKeyWithModifiersCodes;
    std::unordered_set<uint8_t> secondKeyWithModifiersCodes;
    bool state;
    std::set<std::string> _trigger;
    bool isSecondKey;
    std::unordered_set<uint8_t> GetNeededButtons();
public:
    TwoKeysGameFunction(std::unordered_set<uint8_t> firstKeyWithModifiers, std::unordered_set<uint8_t> secondKeyWithModifiers,  std::set<std::string> trigger);
    void SetState(bool newState);
    std::set<std::string> GetTrigger();
};
#endif
