#include "timer-keys-game-function.h"

#include <utility>
#include "../WAAPI/KeyPressSimulator.h"

TimerKeysGameFunction::TimerKeysGameFunction(
	std::set<std::string> triggerPrevious, 
	std::set<std::string> triggerNext) : RangeOfKeysGameFunction(std::move(triggerPrevious), std::move(triggerNext), nullptr)
{
	for (unsigned i = 0; i < 5; i++) {
		this->keyCodes.push_back({(uint8_t)('1' + i)});
	}
}

void TimerKeysGameFunction::SetState(bool newState, RangeOfKeysIndexAction indexAction)
{
    if (newState != this->states[indexAction]) {
        if (newState) {
            if (this->index > 0 && indexAction == RangeOfKeysIndexAction::Decrease) {
                this->index--;
            }
            else if (this->index < this->keyCodes.size() - 1 && indexAction == RangeOfKeysIndexAction::Increase) {
                this->index++;
            }
        }
        for (const auto& neededButton : this->GetNeededButtons()) {
            KeyPressSimulator::SimulateState(neededButton, newState);
        }
        this->states[indexAction] = newState;
    }
}
