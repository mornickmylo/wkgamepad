#ifndef MOUSE_KEY_FUNCTION
#define MOUSE_KEY_FUNCTION

#include <string>
#include <set>
#include "../mouse-keys.h"

class MouseKeyGameFunction {
private:
    uint8_t _key;
    bool state;
    std::set<std::string> _trigger;
public:
    MouseKeyGameFunction(MouseKey mouseKey, std::set<std::string> trigger);
    void SetState(bool newState);
    std::set<std::string> GetTrigger();
};
#endif
