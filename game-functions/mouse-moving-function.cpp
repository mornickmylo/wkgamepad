#include "mouse-moving-function.h"
#include <set>
#include "../defaults.h"
#include "../fake-input.h"

MouseMovingFunction::MouseMovingFunction(MouseMovingDirection mouseMovingDirection, double maxSpeed, std::set<std::string> trigger, std::string dependency, std::string curve)
{
    this->_mouseMovingDirection = mouseMovingDirection;
    this->_maxSpeed = maxSpeed;
    this->_dependency = dependency;
    this->_trigger = trigger;
    this->_curve = curve;
    this->calsSpeedFunctions = {
        {"Linear", &MouseMovingFunction::CalcSpeedLinear},
        {"Quadratic", &MouseMovingFunction::CalcSpeedQuadratic},
        {"Cubic", &MouseMovingFunction::CalcSpeedCubic},
        {"QuadraticExtreme", &MouseMovingFunction::CalcSpeedQuadraticExtreme},
        {"Power", &MouseMovingFunction::CalcSpeedPower}
    };
    this->inputMax = MOUSE_MOVING_INPUT_MAX;
}
void MouseMovingFunction::SetState(double newState)
{
    auto speed = (this->*this->calsSpeedFunctions[this->_curve])(newState);
    InputFaker::FakeMouseMove(
        this->_mouseMovingDirection == MouseMovingDirection::Horizontal ? speed : 0,
        this->_mouseMovingDirection == MouseMovingDirection::Vertical ? speed : 0
    );
}

double MouseMovingFunction::CalcSpeedLinear(double value)
{
    return value / (this->inputMax / this->_maxSpeed);
}
double MouseMovingFunction::CalcSpeedQuadratic(double value)
{
    double valueInRangeBetweenMinusOneAndOne = value / this->inputMax;
    return valueInRangeBetweenMinusOneAndOne * valueInRangeBetweenMinusOneAndOne * (value < 0 ? -1 : 1) * this->_maxSpeed;
}
double MouseMovingFunction::CalcSpeedCubic(double value)
{
    double valueInRangeBetweenMinusOneAndOne = value / this->inputMax;
    return valueInRangeBetweenMinusOneAndOne * valueInRangeBetweenMinusOneAndOne * valueInRangeBetweenMinusOneAndOne * this->_maxSpeed;
}
double MouseMovingFunction::CalcSpeedQuadraticExtreme(double value)
{
    double valueInRangeBetweenMinusOneAndOne = value / this->inputMax;
    return valueInRangeBetweenMinusOneAndOne
        * valueInRangeBetweenMinusOneAndOne
        * (valueInRangeBetweenMinusOneAndOne > 0.95 ? 1.5 : 1)
        * (value < 0 ? -1 : 1)
        * this->_maxSpeed;
}
double MouseMovingFunction::CalcSpeedPower(double value)
{
    return value / (this->inputMax / this->_maxSpeed);
}
std::set<std::string> MouseMovingFunction::GetTrigger()
{
    return this->_trigger;
}
std::string MouseMovingFunction::GetDependency()
{
    return this->_dependency;
}

