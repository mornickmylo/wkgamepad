#include "weapon-keys-game-function.h"
#include <windows.h>

#include <utility>
#include "../WAAPI/KeyPressSimulator.h"

WeaponKeysGameFunction::WeaponKeysGameFunction(
    std::set<std::string> triggerPrevious, 
    std::set<std::string> triggerNext,
    std::unique_ptr<std::set<std::string>> triggerSame) : RangeOfKeysGameFunction(std::move(triggerPrevious), std::move(triggerNext), std::move(triggerSame))
{
    this->keyCodes = { {(uint8_t)VkKeyScanA('`')} };
	for (unsigned i = 0; i < 12; i++) {
        this->keyCodes.push_back(std::vector<uint8_t>{(uint8_t)(VK_F1 + i)});
	}
}

void WeaponKeysGameFunction::SetState(bool newState, RangeOfKeysIndexAction indexAction)
{
    if (newState != this->states[indexAction]) {
        if (newState) {
            if (this->index > 0 && indexAction == RangeOfKeysIndexAction::Decrease) {
                this->index--;
            }
            else if (this->index < this->keyCodes.size() - 1 && indexAction == RangeOfKeysIndexAction::Increase) {
                this->index++;
            }
        }
        for (const auto& neededButton : this->GetNeededButtons()) {
            KeyPressSimulator::SimulateState(neededButton, newState);
        }
        this->states[indexAction] = newState;
    }
}
