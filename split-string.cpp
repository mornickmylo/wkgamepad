#include <vector>
#include <string>

std::vector<std::string> SplitString(std::string s, const std::string& delimiter)
{
    std::vector<std::string> parts;
    size_t pos = 0;
    std::string token;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        parts.push_back(token);
        s.erase(0, pos + delimiter.length());
    }
    parts.push_back(s);
    return parts;
}
