#include <string>
#include <set>
#include <vector>
#include <map>
#include "game-functions/range-of-keys-game-function.h"
#include "game-functions/two-keys-game-function.h"
#include "game-functions/single-key-game-function.h"
#include "game-functions/mouse-moving-function.h"
#include "game-functions/mouse-key-function.h"
#include "game-functions/weapon-keys-game-function.h"
#include "game-functions/timer-keys-game-function.h"
#include <windows.h>
#include <regex>

enum MappingType {
    UnknownMappingName,
    GamepadBoolState,
    GamepadNumericState,
    GamepadNumericStateNumericSetting,
    GamepadNumericStateCurveSetting,
    DebugSetting
};

struct MappingRegexes
{
    std::regex pressingMappingRegex;
    std::regex numericValueMappingRegex;
};

class GamepadStateHandler {
private:
    std::map<MappingType, std::set<std::string>> functionNames;
    std::map<std::string, std::vector<std::string>> mappings;
    std::map<std::string, std::string> curveSettings;
    std::map<std::string, double> mouseSpeedSettings;
    std::map<std::string, std::unordered_set<uint8_t>> singleKeyFunctionKeySyms;
    std::unique_ptr<WeaponKeysGameFunction> pWeaponsFunction;
    std::unique_ptr<TimerKeysGameFunction> pTimerFunction;
    std::unique_ptr<TwoKeysGameFunction> pCameraToggle;
    std::unique_ptr<TwoKeysGameFunction> pGrenadeBounceToggle;
    std::vector<SingleKeyGameFunction> singleKeyFunctions;
    std::vector<MouseMovingFunction> mouseMovingFunctions;
    std::vector<MouseKeyGameFunction> mouseKeyFunctions;
    MappingRegexes mappingRegexes;

    static bool IsTriggerActive(const std::set<std::string>& gamepadState, std::set<std::string>&& trigger);
    MappingType getMappingNameType(const std::string& mappingKey);
    static std::set<std::string> toSet(const std::vector<std::string>& vec);
    static std::string GetError(MappingType mappingType, const std::string& strWithMistake);
    std::string AddMapping(const std::string& functionKey, const std::string& mappingString);
    std::string FinishMapping();
    void SetMappingRegexes();

public:
    GamepadStateHandler();
    std::string Init(const std::map<std::string, std::string>& config);
    void OnGamepadStateChanged(const std::set<std::string>& gamepadBoolState, const std::map<std::string, int16_t>& gamepadNumericState);
};
