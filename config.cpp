#include "config.h"
#include <set>
#include "defaults.h"
#include <iostream>
#include "gamepad-state-handler.h"
#include <fstream>
#include "split-string.h"


std::map<std::string, std::string> Config::ReadConfig(const wchar_t* configPath)
{
    std::fstream configFile(configPath);
    std::string configLine;
    std::map<std::string, std::string> config;
    if (!configFile.is_open()) {
        std::cout << "Error reading config file wkGamepad.config. Ensure file exists" << '\n';
    }
    else 
    {
        while (getline(configFile, configLine)) {
            auto configKeyValuePair = SplitString(configLine, "=");
            if (configKeyValuePair.size() >= 2) {
                config[configKeyValuePair[0]] = configKeyValuePair[1];
            }
        }
    }
    configFile.close();
    return config;
}