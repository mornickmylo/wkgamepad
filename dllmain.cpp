#include <iostream>
#include <set>
#include <map>
#include <vector>
#include <sstream>
#include <iterator>
#include "gamepad-state-handler.h"
#include "SDLAPI/sdl-api.h"
#include "config.h"
#include <windows.h>
#include "WAAPI/Hooker.h"
#include "WAAPI/KeyPressSimulator.h"
#include "debugging/DevConsole.h"
#include "DirectInput/direct-input-adapter.h"
#include "DirectInput/game-window-getter.h"
#include <thread>
#include <chrono>

bool dllShallQuit = false;

using namespace std::chrono_literals;

DWORD __stdcall install_and_run(void*) {
    HWND gameWindow = NULL;
    while (gameWindow == NULL)
    {
        std::this_thread::sleep_for(1000ms);
        GetGameWindow(&gameWindow);
    }
    auto config = Config::ReadConfig(L"wkGamepad.config");
    if (config.count("DEBUG"))
    {
        DevConsole::install();
    }

    bool dinputError = false;
    auto dinputAdapter = std::make_unique<DirectInputAdapter>(dinputError, 0, gameWindow);

    if (dinputError) {
        return 1;
    }

    GamepadStateHandler handler;
    std::string handlerError = handler.Init(config);

    if (!handlerError.empty()) {
        std::cout << handlerError << std::endl;
        return 1;
    }
    auto pHooker = std::make_unique<Hooker>();
    auto pPatternCache = std::make_unique<PatternCache>();
    KeyPressSimulator::install(pHooker, pPatternCache);

    auto start = std::chrono::high_resolution_clock::now();
    std::chrono::microseconds passed;
    auto end = std::chrono::high_resolution_clock::now();
    while (!dllShallQuit) {
        //sdlResult = sdlApi.PollGamepadEvent();
        auto dinputGamepadState = dinputAdapter->GetGamepadState();
        end = std::chrono::high_resolution_clock::now();
        auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        //printf("Microseconds passed: %d\n", (int)microseconds.count());
        start = end;
        if (dinputGamepadState.shallReturn) {
            printf("quitting");
            return 0;
        }
        if (dinputGamepadState.shallHandle) {
            handler.OnGamepadStateChanged(dinputGamepadState.gamepadBoolState, dinputGamepadState.gamepadNumericState);
        }
    }
    printf("quitting");
    return 0;
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
        case DLL_PROCESS_ATTACH: {
            dllShallQuit = false;
            CreateThread(nullptr, 0, install_and_run, nullptr, 0, nullptr);
        }
        break;
        case DLL_THREAD_ATTACH: {
        }
        break;
        case DLL_PROCESS_DETACH: {
            dllShallQuit = true;
        }
        break;
        default:
            return TRUE;
    }
    return TRUE;
}

