#pragma once
#include <string>
#include <map>
#include <windows.h>

class PatternCache
{
private:
	std::map<std::string, DWORD> _patternCache;
public:
	DWORD GetObjAddr(const char* name, const char* pattern, const char* mask);
};

