#include <cstdint>
#include "Hooker.h"
#include <windows.h>
#include "PatternCache.h"

class KeyPressSimulator
{
private:
	static uint8_t* __stdcall onReadKeyboardState(int a1);
	static DWORD origReadKeyboardState;
	static DWORD orig_get_or_set_val_in_keyboard_tables;
	static uint8_t* pFirstKeysTable;
	static uint8_t* pSecondKeysTable;
	static void __stdcall on_get_or_set_val_in_keyboard_tables(int pKeyTablesContainer);
	static DWORD addr_window_procedure;
	static HWND gameWindow;

public:
	static void SimulateState(uint8_t key, bool newState);
	static void install(const std::unique_ptr<Hooker>& pHooker, const std::unique_ptr<PatternCache>& pPatternCache);
};