#include <vector>
#include <string>
#include <polyhook2/Enums.hpp>
#include <polyhook2/Detour/x86Detour.hpp>
#include <windows.h>

#pragma once
class Hooker
{
private:
	std::vector<std::string> alreadyHookedNames;
	std::vector<DWORD> alreadyHookedAddresses;
	std::vector<std::unique_ptr<PLH::x86Detour>> detoursStorage;
    BOOL InsertJump(PVOID pDest, size_t dwPatchSize, PVOID pCallee, DWORD dwJumpType);
    BOOL PatchMemData(PVOID pAddr, size_t buf_len, PVOID pNewData, size_t data_len);
    //Worms development tools by StepS
    template<typename VT>
    static BOOL __stdcall PatchMemVal(PVOID pAddr, VT newValue) { return PatchMemData(pAddr, sizeof(VT), &newValue, sizeof(VT)); }
    template<typename VT>
    static BOOL __stdcall PatchMemVal(ULONG_PTR pAddr, VT newValue) { return PatchMemData((PVOID) pAddr, sizeof(VT), &newValue, sizeof(VT)); }

public:
	Hooker();
	void hook(const std::string& name, DWORD pTarget, DWORD* pDetour, DWORD* ppOriginal);
    void hookAsm(DWORD startAddr, DWORD hookAddr, const char * line)
};

