#include "Hooker.h"
#include <stdexcept>
#include <sstream>
#include <polyhook2/Detour/x86Detour.hpp>

Hooker::Hooker()
= default;

#define IJ_JUMP 0 //Insert a jump (0xE9) with InsertJump
#define IJ_CALL 1 //Insert a call (0xE8) with InsertJump
#define IJ_FARJUMP 2 //Insert a farjump (0xEA) with InsertJump
#define IJ_FARCALL 3 //Insert a farcall (0x9A) with InsertJump
#define IJ_PUSHRET 4 //Insert a pushret with InsertJump

void Hooker::hook(const std::string& name, DWORD pTarget, DWORD* pDetour, DWORD* ppOriginal)
{
	if (!pTarget)
		throw std::runtime_error("Hook adress is null: " + name);
	if (std::find(alreadyHookedNames.begin(), alreadyHookedNames.end(), name) != alreadyHookedNames.end())
		throw std::runtime_error("Hook name reused: " + name);
	if (std::find(alreadyHookedAddresses.begin(), alreadyHookedAddresses.end(), pTarget) != alreadyHookedAddresses.end()) {
		std::stringstream ss;
		ss << "The specified address is already hooked: " << name << "(0x" << std::hex << pTarget << "), ";
		throw std::runtime_error(ss.str());
	}

	uint64_t trampoline = 0;
	auto detour = std::make_unique<PLH::x86Detour>(pTarget, (const uint64_t)pDetour, &trampoline);
	auto res = detour->hook();
	auto arch = detour->getArchType();
	if (!res) {
		throw std::runtime_error("Failed to create hook: " + name);
	}
	this->detoursStorage.push_back(std::move(detour));
	*ppOriginal = (DWORD)trampoline;

	alreadyHookedNames.push_back(name);
	alreadyHookedAddresses.push_back(pTarget);

}
void Hooker::hookAsm(DWORD startAddr, DWORD hookAddr, const char * line) {
    if(!line) {
        printf("hookAsm: 0x%X -> 0x%X\n", startAddr, hookAddr);
    } else {
        printf("%s hookAsm: 0x%X -> 0x%X\n", line, startAddr, hookAddr);
    }
    alreadyHookedNames.emplace_back((const char*)startAddr, 6);
    alreadyHookedAddresses.push_back(startAddr);
    InsertJump((PVOID)startAddr, 6, (PVOID)hookAddr, IJ_PUSHRET);
}

BOOL Hooker::InsertJump(PVOID pDest, size_t dwPatchSize, PVOID pCallee, DWORD dwJumpType) {
    if (dwPatchSize >= 5 && pDest) {
        DWORD OpSize = 5, OpCode = 0xE9;
        PBYTE dest = (PBYTE) pDest;
        switch (dwJumpType) {
            case IJ_PUSHRET:
                OpSize = 6;
                OpCode = 0x68;
                break;
            case IJ_FARJUMP:
                OpSize = 7;
                OpCode = 0xEA;
                break;
            case IJ_FARCALL:
                OpSize = 7;
                OpCode = 0x9A;
                break;
            case IJ_CALL:
                OpSize = 5;
                OpCode = 0xE8;
                break;
            default:
                OpSize = 5;
                OpCode = 0xE9;
                break;
        }
        if (dwPatchSize < OpSize)
            return 0;
        PatchMemVal(dest, (BYTE) OpCode);
        switch (OpSize) {
            case 7:
                PatchMemVal(dest + 1, pCallee);
                WORD w_cseg;
                __asm mov[w_cseg], cs;
                PatchMemVal(dest + 5, w_cseg);
                break;
            case 6:
                PatchMemVal(dest + 1, pCallee);
                PatchMemVal<BYTE>(dest + 5, 0xC3);
                break;
            default:
                PatchMemVal(dest + 1, (ULONG_PTR) pCallee - (ULONG_PTR) pDest - 5);
                break;
        }
        for (size_t i = OpSize; i < dwPatchSize; i++)
            PatchMemVal<BYTE>(dest + i, 0x90);
    }
    return 0;
}

BOOL Hooker::PatchMemData(PVOID pAddr, size_t buf_len, PVOID pNewData, size_t data_len) {
    if (!buf_len || !data_len || !pNewData || !pAddr || buf_len < data_len) {
        SetLastError(ERROR_INVALID_PARAMETER);
        return 0;
    }
    DWORD dwLastProtection;
    if (!VirtualProtect((void *) pAddr, data_len, PAGE_EXECUTE_READWRITE, &dwLastProtection))
        return 0;
    memcpy_s(pAddr, buf_len, pNewData, data_len);
    return VirtualProtect((void *) pAddr, data_len, dwLastProtection, &dwLastProtection);
}