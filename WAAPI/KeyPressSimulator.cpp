#include "KeyPressSimulator.h"
#include "../fake-input.h"

DWORD KeyPressSimulator::origReadKeyboardState = 0;
DWORD KeyPressSimulator::orig_get_or_set_val_in_keyboard_tables = 0;
uint8_t * KeyPressSimulator::pFirstKeysTable = nullptr;
uint8_t* KeyPressSimulator::pSecondKeysTable = nullptr;
HWND KeyPressSimulator::gameWindow = nullptr;
DWORD KeyPressSimulator::addr_window_procedure = 0;

void KeyPressSimulator::SimulateState(uint8_t key, bool newState)
{
	if (key == VK_LBUTTON || key == VK_RBUTTON) {
		InputFaker::FakeMouse(key == VK_LBUTTON ? MouseKey::Left : MouseKey::Right, newState);
	} else if (pFirstKeysTable) {
		if (newState) {
			pFirstKeysTable[key] = 1;
		} else {
			if (GetKeyState(key) < 0) {
				pFirstKeysTable[key] = 1;
			} else {
				pFirstKeysTable[key] = 0;
				pSecondKeysTable[key] = 0;
			}
		}
	} 
}

uint8_t* __stdcall KeyPressSimulator::onReadKeyboardState(int a1)
{
	pFirstKeysTable = (uint8_t*)(a1 + 284);
	pSecondKeysTable = (uint8_t*)(a1 + 540);
#ifdef _MSC_VER
    __asm push a1
	__asm call origReadKeyboardState
#elif __GNUC__ || __clang__
    __asm__("push %0" : : "m" (a1));
    __asm__("call %0" : : "X"(origReadKeyboardState));
#endif
    return pSecondKeysTable;
}

int callsCount;
void __stdcall KeyPressSimulator::on_get_or_set_val_in_keyboard_tables(int pKeyTablesContainer)
{
	int value_to_set = 0;
	int result = 0;
	uint8_t keyCode = 0;
	uint8_t* pKeySysStates = (uint8_t*)(pKeyTablesContainer + 284);
#ifdef _MSC_VER
	__asm mov value_to_set, edx
	__asm mov keyCode, al
#elif __GNUC__ || __clang__
    __asm__("mov %0, edx" : "=rm" (value_to_set));
    __asm__("mov %0, al" : "=rm" (keyCode));
#endif

    //*(uint8_t*)(pKeyTablesContainer + keyCode + 540) = *pKeySysstate | KeyPressSimulator::simulatedKeyStates[keyCode];
#ifdef _MSC_VER
    __asm mov edx, value_to_set
	__asm mov al, keyCode
	__asm push pKeyTablesContainer
	__asm call orig_get_or_set_val_in_keyboard_tables
	__asm mov result, eax

	__asm mov eax, result
#elif __GNUC__ || __clang__
    __asm__("mov edx, %0" : : "rm" (value_to_set));
    __asm__("mov al, %0" : : "rm" (keyCode));
    __asm__("push %0" : : "m" (pKeyTablesContainer));
    __asm__("call %0" : : "X"(orig_get_or_set_val_in_keyboard_tables));
    __asm__("mov %0, eax" : "=rm" (result));

    __asm__("mov eax, %0" : : "rm" (result));
#endif
}

void KeyPressSimulator::install(const std::unique_ptr<Hooker>& pHooker, const std::unique_ptr<PatternCache>& pPatternCache)
{
	DWORD addrChatInput = pPatternCache->GetObjAddr("ReadKeyboardState", "\x83\xEC\x1C\x56\x8B\x35\x00\x00\x00\x00", "xxxxxx????");
	pHooker->hook("ReadKeyboardState",
		addrChatInput,
		(DWORD*)&KeyPressSimulator::onReadKeyboardState,
		(DWORD*)&KeyPressSimulator::origReadKeyboardState);
	DWORD addr_get_or_set_val_in_keyboard_tables = pPatternCache->GetObjAddr("get_or_set_val_in_keyboard_tables", "\xE8\x00\x00\x00\x00\x0B\xD8", "x????xx");
	pHooker->hook("get_or_set_val_in_keyboard_tables",
		addr_get_or_set_val_in_keyboard_tables,
		(DWORD*)&KeyPressSimulator::on_get_or_set_val_in_keyboard_tables,
		(DWORD*)&KeyPressSimulator::orig_get_or_set_val_in_keyboard_tables);

	addr_window_procedure = pPatternCache->GetObjAddr("window_procedure", "\x55\x8B\xEC\x83\xE4\xF8\x81\xEC\x00\x00\x00\x00\x83\x3D\x00\x00\x00\x00\x00\x53\x56\x8B\x75\x0C", "xxxxxxxx????xx?????xxxxx");
}
