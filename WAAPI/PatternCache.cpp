#include "PatternCache.h"
#include <stdexcept>
#ifdef _MSC_VER
#include <format>
#endif
#include "hacklib/PatternScanner.h"

DWORD PatternCache::GetObjAddr(const char* name, const char* pattern, const char* mask)
{
	auto it = _patternCache.find(name);
	uintptr_t ret = 0;
	if (it != _patternCache.end()) {
		ret = it->second;
	}
	else {
		ret = hl::FindPatternMask(pattern, mask);
		if (ret) {
			_patternCache[name] = ret;
		}
	}
	//some logging logic here
	if (!ret) {
#ifdef _MSC_VER
		auto err_str = std::format("scanPattern: failed to find memory pattern: {}", name);
#else
		auto err_str = std::string("scanPattern: failed to find memory pattern: ") + std::string(name);
#endif
		throw std::runtime_error(err_str);
	}
	return ret;
}
