#include <string>
#include <set>
#include "gamepad-state-handler.h"
#include <algorithm>
#include "defaults.h"
#include "split-string.h"

GamepadStateHandler::GamepadStateHandler()
{
    this->functionNames = {
        {MappingType::GamepadBoolState, {"WEAPON_CLASS_PREVIOUS", "WEAPON_CLASS_NEXT", "WEAPON_CLASS_SAME", "TIMER_REDUCE", "TIMER_INCREASE", "SPACE", "UP", "LEFT", "RIGHT", "DOWN", "ENTER", "SPACE", "SHIFT", "BACKSPACE", "GRENADE_BOUNCE_TOGGLE", "CAMERA_TOGGLE", "RIGHT_MOUSE_KEY", "LEFT_MOUSE_KEY", "TAB", "ESC", "DELETE", "INSERT"}},
        {MappingType::GamepadNumericState, {"MOUSE_HORIZONTAL", "MOUSE_VERTICAL"}},
        {MappingType::GamepadNumericStateNumericSetting, {"MOUSE_HORIZONTAL_MAX_SPEED", "MOUSE_VERTICAL_MAX_SPEED"}},
        {MappingType::GamepadNumericStateCurveSetting, {"MOUSE_HORIZONTAL_CURVE", "MOUSE_VERTICAL_CURVE"}},
        {MappingType::DebugSetting, {"DEBUG"}}
    };
    this->pWeaponsFunction = nullptr;
    this->pTimerFunction = nullptr;
    this->pCameraToggle = nullptr;
    this->pGrenadeBounceToggle = nullptr;
    this->singleKeyFunctionKeySyms = {
        {"UP", {VK_UP}},
        {"LEFT", {VK_LEFT}},
        {"RIGHT", {VK_RIGHT}},
        {"DOWN", {VK_DOWN}},
        {"ENTER", {VK_RETURN}},
        {"SPACE", {VK_SPACE}},
        {"SHIFT", {VK_SHIFT}},
        {"BACKSPACE", {VK_BACK}},
        {"TAB", {VK_TAB}},
        {"ESC", {VK_ESCAPE}},
        {"DELETE", {VK_DELETE}},
        {"INSERT", {VK_INSERT}}

    };
    this->curveSettings = {
        {"MOUSE_HORIZONTAL_CURVE", DEFAULT_MOUSE_MOVE_CURVE},
        {"MOUSE_VERTICAL_CURVE", DEFAULT_MOUSE_MOVE_CURVE},
    };
    this->mouseSpeedSettings = {
        {"MOUSE_HORIZONTAL_MAX_SPEED", DEFAULT_MOUSE_MOVE_MAX_SPEED},
        {"MOUSE_VERTICAL_MAX_SPEED", DEFAULT_MOUSE_MOVE_MAX_SPEED},
    };
}
std::string GamepadStateHandler::GetError(MappingType mappingType, const std::string& strWithMistake)
{
    switch(mappingType) {
        case MappingType::GamepadBoolState:
            return strWithMistake + " is not a string describing pressing";
        break;
        case MappingType::UnknownMappingName:
            return "Unknown key: " + strWithMistake;
        break;
        case MappingType::GamepadNumericState:
            return strWithMistake + " is not a string describing pressing or axis";
        break;
        case MappingType::GamepadNumericStateNumericSetting:
            return strWithMistake + " is not a positive number";
        break;
        case MappingType::GamepadNumericStateCurveSetting:
            return strWithMistake + " is not a curve name";
        break;
        case MappingType::DebugSetting:
            return strWithMistake + " is not a debug setting";
            break;
        default:
            return "";
    }
}

MappingType GamepadStateHandler::getMappingNameType(const std::string& mappingKey) {
    for (auto & functionName : this->functionNames) {
        if (functionName.second.find(mappingKey) != functionName.second.end()) {
            return functionName.first;
        }
    }
    return MappingType::UnknownMappingName;
}
std::string GamepadStateHandler::Init(const std::map<std::string, std::string>& config)
{
    std::string error;
    this->SetMappingRegexes();

    for (const auto& configKeyValuePair : config) {
        error += this->AddMapping(configKeyValuePair.first, configKeyValuePair.second);
        if (!error.empty()) {
            return error;
        }
    }
    error = this->FinishMapping();
    return error;
}
std::string GamepadStateHandler::AddMapping(const std::string& functionKey, const std::string& mappingString)
{
    const auto type = this->getMappingNameType(functionKey);
    if (type == MappingType::UnknownMappingName) {
        return "Unknown key: " + functionKey;
    }
    if (type == MappingType::DebugSetting) {
        return "";
    }
    if (type == MappingType::GamepadBoolState || type == MappingType::GamepadNumericState) {
        std::vector<std::string> parts = SplitString(mappingString, ";");
        bool isMappingStringPartOk;
        for (const auto& part : parts) {
            isMappingStringPartOk = type == MappingType::GamepadBoolState
                ? std::regex_search(part, this->mappingRegexes.pressingMappingRegex)
                : std::regex_search(part, this->mappingRegexes.pressingMappingRegex) || std::regex_search(part, this->mappingRegexes.numericValueMappingRegex);
            if (!isMappingStringPartOk) {
                return this->GetError(type, part);
            }
        }
        this->mappings[functionKey] = parts;
    } else {
        const std::regex settingValueRegex = type == MappingType::GamepadNumericStateNumericSetting
            ? std::regex(R"(^[\d\.]+$)")
            : std::regex(R"(^(Quadratic|Cubic|QuadraticExtreme|Power)$)");
        if (!std::regex_search(mappingString, settingValueRegex)) {
            return this->GetError(type, mappingString);
        }
        if (MappingType::GamepadNumericStateNumericSetting == type) {
            this->mouseSpeedSettings[functionKey] = std::stod(mappingString);
        } else {
            this->curveSettings[functionKey] = mappingString;
        }
    }
    return "";
}
std::string GamepadStateHandler::FinishMapping() {
    for (auto & mapping : this->mappings) {
        if (mapping.first.find("WEAPON_CLASS") != std::string::npos) {
            if (this->pWeaponsFunction != nullptr) {
                continue;
            }
            if (this->mappings["WEAPON_CLASS_PREVIOUS"].empty() || this->mappings["WEAPON_CLASS_NEXT"].empty()) {
                return "When specifying weapons switching, both WEAPON_CLASS_PREVIOUS and WEAPON_CLASS_NEXT must be specified";
            }

            std::vector<std::vector<uint8_t>> weaponClassButtons = {{(uint8_t)VkKeyScanA('`')}};
            auto sameClassTrigger = std::make_unique<std::set<std::string>>(toSet(this->mappings["WEAPON_CLASS_SAME"]));
            for (unsigned i = 0; i < 12; i++) {
                weaponClassButtons.push_back(std::vector<uint8_t>{(uint8_t)(VK_F1+i)});
            }
            this->pWeaponsFunction = std::make_unique<WeaponKeysGameFunction>(
                toSet(this->mappings["WEAPON_CLASS_PREVIOUS"]),
                toSet(this->mappings["WEAPON_CLASS_NEXT"]),
                !sameClassTrigger->empty() ? std::move(sameClassTrigger) : nullptr
            );
        } else if (mapping.first.find("TIMER") != std::string::npos) {
            if (this->pTimerFunction != nullptr) {
                continue;
            }
            if (this->mappings["TIMER_REDUCE"].empty() || this->mappings["TIMER_INCREASE"].empty()) {
                return "When specifying timer switching, both TIMER_REDUCE and TIMER_INCREASE must be specified";
            }

            this->pTimerFunction = std::make_unique<TimerKeysGameFunction>(
                toSet(this->mappings["TIMER_REDUCE"]), 
                toSet(this->mappings["TIMER_INCREASE"])
            );

        } else if (mapping.first == "CAMERA_TOGGLE") {
            this->pCameraToggle = std::make_unique<TwoKeysGameFunction>(
                std::unordered_set<uint8_t>{VK_HOME},
                std::unordered_set<uint8_t>{VK_RCONTROL, VK_HOME},
                toSet(this->mappings["CAMERA_TOGGLE"])
            );
        } else if (mapping.first == "GRENADE_BOUNCE_TOGGLE") {
            this->pGrenadeBounceToggle = std::make_unique<TwoKeysGameFunction>(
                std::unordered_set<uint8_t>{VK_OEM_PLUS},
                std::unordered_set<uint8_t>{VK_OEM_MINUS},
                toSet(this->mappings["GRENADE_BOUNCE_TOGGLE"])
            );
        } else if (mapping.first == "MOUSE_HORIZONTAL" || mapping.first == "MOUSE_VERTICAL") {
            std::smatch axisMatch;
            std::regex axisRegex(R"(^Axis(R?[XYZ])$)");
            std::set<std::string> trigger;
            std::string axis;
            for (const auto& mappedItem : this->mappings[mapping.first]) {
                if (std::regex_search(mappedItem, axisMatch, axisRegex)) {
                    axis = axisMatch.str(1);
                } else {
                    trigger.insert(mappedItem);
                }
            }
            this->mouseMovingFunctions.emplace_back(
                    mapping.first == "MOUSE_HORIZONTAL" ? MouseMovingDirection::Horizontal : MouseMovingDirection::Vertical,
                    this->mouseSpeedSettings[mapping.first + "_MAX_SPEED"],
                    trigger,
                    axis,
                    this->curveSettings[mapping.first + "_CURVE"]
            );
        } else if (mapping.first == "LEFT_MOUSE_KEY" || mapping.first == "RIGHT_MOUSE_KEY") {
            this->mouseKeyFunctions.emplace_back(
                    mapping.first == "LEFT_MOUSE_KEY" ? MouseKey::Left : MouseKey::Right,
                    toSet(this->mappings[mapping.first])
            );
        } else {
            auto functionKeySyms = singleKeyFunctionKeySyms[mapping.first];
            if (!functionKeySyms.empty()) {
                this->singleKeyFunctions.emplace_back(
                        functionKeySyms,
                        toSet(this->mappings[mapping.first])
                );
            }
        }
    }
    return "";
}
void GamepadStateHandler::SetMappingRegexes()
{
    this->mappingRegexes.pressingMappingRegex = std::regex(R"(^Button\d+$|^AxisR?[XYZ][+-]|^POV[UDRL]$)");
    this->mappingRegexes.numericValueMappingRegex = std::regex(R"(^AxisR?[XYZ]$)");
}
std::set<std::string> GamepadStateHandler::toSet(const std::vector<std::string>& vec)
{
    std::set<std::string> set;
    for (const auto& elem : vec) {
        set.insert(elem);
    }
    return set;
}
void GamepadStateHandler::OnGamepadStateChanged(const std::set<std::string>& gamepadBoolState, const std::map<std::string, int16_t>& gamepadNumericState)
{
    for (auto& singleKeyFunction : this->singleKeyFunctions) {
        singleKeyFunction.SetState(IsTriggerActive(gamepadBoolState, singleKeyFunction.GetTrigger()));
    }
    if (this->pWeaponsFunction != nullptr) {
        if (this->pWeaponsFunction->HasTriggerSame()) {
            this->pWeaponsFunction->SetState(IsTriggerActive(gamepadBoolState, this->pWeaponsFunction->GetTriggerSame()), RangeOfKeysIndexAction::Stay);
        }
        this->pWeaponsFunction->SetState(IsTriggerActive(gamepadBoolState, this->pWeaponsFunction->GetTriggerPrevious()), RangeOfKeysIndexAction::Decrease);
        this->pWeaponsFunction->SetState(IsTriggerActive(gamepadBoolState, this->pWeaponsFunction->GetTriggerNext()), RangeOfKeysIndexAction::Increase);
    }
    if (this->pTimerFunction != nullptr) {
        this->pTimerFunction->SetState(IsTriggerActive(gamepadBoolState, this->pTimerFunction->GetTriggerPrevious()), RangeOfKeysIndexAction::Decrease);
        this->pTimerFunction->SetState(IsTriggerActive(gamepadBoolState, this->pTimerFunction->GetTriggerNext()), RangeOfKeysIndexAction::Increase);
    }
    if (this->pCameraToggle != nullptr) {
        this->pCameraToggle->SetState(IsTriggerActive(gamepadBoolState, this->pCameraToggle->GetTrigger()));
    }
    if (this->pGrenadeBounceToggle != nullptr) {
        this->pGrenadeBounceToggle->SetState(IsTriggerActive(gamepadBoolState, this->pGrenadeBounceToggle->GetTrigger()));
    }
    for (auto& mouseMovingFunction : this->mouseMovingFunctions) {
        if (gamepadNumericState.at(mouseMovingFunction.GetDependency()) == 0 || IsTriggerActive(gamepadBoolState, mouseMovingFunction.GetTrigger())) {
            mouseMovingFunction.SetState(gamepadNumericState.at(mouseMovingFunction.GetDependency()));
        }
    }
    for (auto& mouseKeyFunction : this->mouseKeyFunctions) {
        mouseKeyFunction.SetState(IsTriggerActive(gamepadBoolState, mouseKeyFunction.GetTrigger()));
    }
}
bool GamepadStateHandler::IsTriggerActive(const std::set<std::string>& gamepadState, std::set<std::string>&& trigger)
{
    return std::ranges::all_of(trigger, [&gamepadState](auto& gamepadButtonOrAxisState) {
        return std::find(gamepadState.begin(), gamepadState.end(), gamepadButtonOrAxisState) != gamepadState.end();
    });
}

