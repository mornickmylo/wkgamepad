#include "sdl-api.h"
#include <SDL.h>
#include <iostream>

SDLAPI::SDLAPI(bool& initError) {
    initError = true;
    this->gGameController = nullptr;
    this->triggers = nullptr;
    if (SDL_Init(SDL_INIT_JOYSTICK) < 0)
    {
        std::cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << std::endl;
    }
    else {
        std::cout << "SDL initialized!" << std::endl;
        int gamepadsAmount = SDL_NumJoysticks();
        std::cout << "Gamepads amount: " << gamepadsAmount << std::endl;
        if (gamepadsAmount > 0) {
            this->gGameController = SDL_JoystickOpen(0);
            if (this->gGameController == nullptr)
            {
                std::cout << "Warning: Unable to open game controller! SDL Error: " << SDL_GetError() << std::endl;
            }
            else {
                initError = false;
                this->getTriggers();
            }
        }
    }
}

void SDLAPI::getTriggers() {
    const int joystickNumAxes = SDL_JoystickNumAxes(this->gGameController);
    std::cout << "Gamepad triggers: " << std::endl;
    this->triggers = new std::set<int>;
    Sint16 axisState;
    for (int axis = 0; axis < joystickNumAxes; axis++) {
        axisState = SDL_JoystickGetAxis(this->gGameController, axis);
        std::cout << "Axis " << axis << " state: " << axisState << std::endl;
        if (axisState < -32000) {
            this->triggers->insert(axis);
            std::cout << axis << std::endl;
        }
    }
}

SdlEventHandleResult SDLAPI::PollGamepadEvent()
{
    static SDL_Event e;
    if ((SDL_PollEvent(&e) == 0)) {
        return {
            nullptr,
            nullptr,
            false,
            false
        };
    }
    if (e.type == SDL_EventType::SDL_QUIT)
    {
        SDL_JoystickClose(gGameController);
        return {
            nullptr,
            nullptr,
            true,
            false
        };
    }
    else if (this->handledGamepadEvents.find(e.type) == this->handledGamepadEvents.end())
    {
        return {
            nullptr,
            nullptr,
            false,
            false
        };
    }
    if (this->triggers == nullptr) {
        this->getTriggers();
    }
    static std::set<std::string> gamepadBoolState;
    static std::map<uint8_t, int16_t> gamepadNumericState;
    if (e.type == SDL_EventType::SDL_JOYHATMOTION)
    {
        std::cout << "Hat value: " << unsigned(e.jhat.value) << std::endl;
    }
    else if (e.type == SDL_EventType::SDL_JOYAXISMOTION)
    {
        const std::string axisStr = std::to_string(e.jaxis.axis);
        if (triggers->find(e.jaxis.axis) != triggers->end()) {
            if (e.jaxis.value > 0) {
                gamepadBoolState.insert("Trigger" + axisStr);
            } else {
                gamepadBoolState.erase("Trigger" + axisStr);
            }
        }
        else {
            if (e.jaxis.value > this->joystickBoolStateDeadZone) {
                gamepadBoolState.insert("Axis" + axisStr + "+");
                gamepadBoolState.erase("Axis" + axisStr + "-");
            }
            else if (e.jaxis.value < -this->joystickBoolStateDeadZone) {
                gamepadBoolState.erase("Axis" + axisStr + "+");
                gamepadBoolState.insert("Axis" + axisStr + "-");
            }
            else {
                gamepadBoolState.erase("Axis" + axisStr + "+");
                gamepadBoolState.erase("Axis" + axisStr + "-");
            }
            if (e.jaxis.value > this->joystickNumericStateDeadZone || e.jaxis.value < -this->joystickNumericStateDeadZone) {
                gamepadNumericState[e.jaxis.axis] = e.jaxis.value;
            }
            else {
                gamepadNumericState[e.jaxis.axis] = 0;
            }
        }
    }
    else if (e.type == SDL_EventType::SDL_JOYBUTTONDOWN || e.type == SDL_EventType::SDL_JOYBUTTONUP)
    {
        const bool isPressed = e.type != SDL_EventType::SDL_JOYBUTTONUP;
        const std::string btnStr = "Button" + std::to_string(e.jbutton.button);
        if (isPressed) {
            gamepadBoolState.insert(btnStr);
        }
        else {
            gamepadBoolState.erase(btnStr);
        }
    }

    SDLAPI::logStringSetIfChanged(gamepadBoolState);
    return {
        &gamepadBoolState,
        &gamepadNumericState,
        false,
        true
    };
}

void SDLAPI::logStringSetIfChanged(const std::set<std::string>& set) {
    static std::string setStr;
    std::string newSetStr;
    for (const std::string& str : set) {
        newSetStr += str + ";";
    }
    if (newSetStr != setStr) {
        setStr = newSetStr;
        std::cout << (setStr.empty() ? "---" : setStr) << std::endl;
    }
}

SDLAPI::~SDLAPI() {
    SDL_Quit();
}