#include <set>
#include <string>
#include "sdl-event-handle-result.h"
#include <SDL.h>
#include "../defaults.h"

class SDLAPI {
private:
	std::set<int>* triggers;
	void getTriggers();
	static void logStringSetIfChanged(const std::set<std::string>& set);
	SDL_Joystick* gGameController;
	int joystickBoolStateDeadZone = DEFAULT_JOYSTICK_BOOL_STATE_DEADZONE;
	int joystickNumericStateDeadZone = DEFAULT_JOYSTICK_BOOL_STATE_DEADZONE;
	const std::set<Uint32> handledGamepadEvents{ SDL_EventType::SDL_JOYAXISMOTION, SDL_EventType::SDL_JOYBUTTONDOWN, SDL_EventType::SDL_JOYBUTTONUP, SDL_EventType::SDL_JOYHATMOTION };
public:
	SdlEventHandleResult PollGamepadEvent();
	explicit SDLAPI(bool& initError);
	~SDLAPI();
};