#include <map>

typedef struct {
	std::set<std::string>* gamepadBoolState;
	std::map<uint8_t, int16_t>* gamepadNumericState;
	bool shallReturn;
	bool shallHandle;
} SdlEventHandleResult;