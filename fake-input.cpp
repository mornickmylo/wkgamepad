#include "fake-input.h"
#include <windows.h>
#include <stdint.h>

void InputFaker::FakeOne(uint8_t key, bool state)
{
	INPUT inputs[1] = {};
	ZeroMemory(inputs, sizeof(inputs));

	inputs[0].type = INPUT_KEYBOARD;
	inputs[0].ki.wVk = key;
	if (!state)
	{
		inputs[0].ki.dwFlags = KEYEVENTF_KEYUP;
	}

	SendInput(1, inputs, sizeof(INPUT));
}

void InputFaker::FakeMouse(MouseKey mouseKey, bool state)
{
	INPUT inputs[1] = {};
	ZeroMemory(inputs, sizeof(inputs));

	inputs[0].type = INPUT_MOUSE;
	inputs[0].mi.dwFlags = MOUSEEVENTF_ABSOLUTE;
	if (mouseKey == MouseKey::Left) {
		if (state) {
			inputs[0].mi.dwFlags |= MOUSEEVENTF_LEFTDOWN;
		}
		else {
			inputs[0].mi.dwFlags |= MOUSEEVENTF_LEFTUP;
		}
	} else if (mouseKey == MouseKey::Right) {
		if (state) {
			inputs[0].mi.dwFlags |= MOUSEEVENTF_RIGHTDOWN;
		}
		else {
			inputs[0].mi.dwFlags |= MOUSEEVENTF_RIGHTUP;
		}
	}

	SendInput(1, inputs, sizeof(INPUT));
}

void InputFaker::FakeMouseMove(LONG dx, LONG dy)
{
	INPUT inputs[1] = {};
	ZeroMemory(inputs, sizeof(inputs));

	inputs[0].type = INPUT_MOUSE;
	inputs[0].mi.dx = dx;
	inputs[0].mi.dy = dy;
	inputs[0].mi.dwFlags = MOUSEEVENTF_MOVE;

	SendInput(1, inputs, sizeof(INPUT));

}