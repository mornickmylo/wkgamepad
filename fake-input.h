#include <windows.h>
#include <cstdint>
#include "mouse-keys.h"

class InputFaker
{
public:
	static void FakeOne(uint8_t key, bool state);
	static void FakeMouse(MouseKey mouseKey, bool state);
	static void FakeMouseMove(LONG dx, LONG dy);
private:
};